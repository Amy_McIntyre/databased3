﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace d2
{
    //Student Name: Amy McIntyre
    //Student ID: 9999413
    class SQL
    {

        public static SqlConnection con = new SqlConnection(@"Data Source=BCS303A-08A;Database=Deliverable2_AmyMcIntyre;Integrated Security=True");
        //public static string connstring = @"Data Source=jk11-mstest.database.windows.net;Initial Catalog=d2sql;User ID=jk11;Password=Test1234";
        //public static SqlConnection con = new SqlConnection(connstring);

        public static SqlCommand cmd = new SqlCommand();
        public static SqlDataReader read;

        public static void executeQuery(string query)
        {
            try
            {
                con.Close();
                cmd.Connection = con;
                con.Open();
                cmd.CommandText = query;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return;
            }
        }


        public static void selectQuery(string query)
        {
            try
            {
                con.Close();
                cmd.Connection = con;
                con.Open();
                cmd.CommandText = query;
                read = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                return;
            }
        }

        public static void editComboBoxItems(ComboBox comboBox, string query)
        {
            bool clear = true;
            
            SQL.selectQuery(query);
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (comboBox.Text == SQL.read[0].ToString())
                    {
                        clear = false;
                    }
                }
            }
            
            SQL.selectQuery(query);
            if (clear)
            {
                comboBox.Text = "";
                comboBox.Items.Clear();

            }
            
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    comboBox.Items.Add(SQL.read[0].ToString());
                }
            }
        }
    }
}
