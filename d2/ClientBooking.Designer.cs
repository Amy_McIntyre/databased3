﻿namespace d2
{
    partial class ClientBooking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.instructor_box = new System.Windows.Forms.ComboBox();
            this.submit_btn = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.fourth_lesson = new System.Windows.Forms.Button();
            this.second_lesson = new System.Windows.Forms.Button();
            this.thrid_lesson = new System.Windows.Forms.Button();
            this.first_lesson = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(226, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Please fill out this form to book an appointment";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Client Name";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 327);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(131, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Please select an instructor";
            // 
            // instructor_box
            // 
            this.instructor_box.FormattingEnabled = true;
            this.instructor_box.Location = new System.Drawing.Point(188, 324);
            this.instructor_box.Name = "instructor_box";
            this.instructor_box.Size = new System.Drawing.Size(121, 21);
            this.instructor_box.TabIndex = 12;
            this.instructor_box.SelectedIndexChanged += new System.EventHandler(this.instructor_box_SelectedIndexChanged);
            // 
            // submit_btn
            // 
            this.submit_btn.Location = new System.Drawing.Point(188, 387);
            this.submit_btn.Name = "submit_btn";
            this.submit_btn.Size = new System.Drawing.Size(75, 23);
            this.submit_btn.TabIndex = 13;
            this.submit_btn.Text = "Submit";
            this.submit_btn.UseVisualStyleBackColor = true;
            this.submit_btn.Click += new System.EventHandler(this.submit_btn_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(188, 88);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 20);
            this.textBox1.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "ID";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(117, 148);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 34;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // fourth_lesson
            // 
            this.fourth_lesson.Location = new System.Drawing.Point(323, 223);
            this.fourth_lesson.Name = "fourth_lesson";
            this.fourth_lesson.Size = new System.Drawing.Size(94, 39);
            this.fourth_lesson.TabIndex = 40;
            this.fourth_lesson.Text = "3pm-4pm";
            this.fourth_lesson.UseVisualStyleBackColor = true;
            this.fourth_lesson.Click += new System.EventHandler(this.fourth_lesson_Click);
            // 
            // second_lesson
            // 
            this.second_lesson.Location = new System.Drawing.Point(123, 223);
            this.second_lesson.Name = "second_lesson";
            this.second_lesson.Size = new System.Drawing.Size(94, 39);
            this.second_lesson.TabIndex = 39;
            this.second_lesson.Text = "11am-12pm";
            this.second_lesson.UseVisualStyleBackColor = true;
            this.second_lesson.Click += new System.EventHandler(this.second_lesson_Click);
            // 
            // thrid_lesson
            // 
            this.thrid_lesson.Location = new System.Drawing.Point(223, 223);
            this.thrid_lesson.Name = "thrid_lesson";
            this.thrid_lesson.Size = new System.Drawing.Size(94, 39);
            this.thrid_lesson.TabIndex = 38;
            this.thrid_lesson.Text = "1pm-2pm";
            this.thrid_lesson.UseVisualStyleBackColor = true;
            this.thrid_lesson.Click += new System.EventHandler(this.thrid_lesson_Click);
            // 
            // first_lesson
            // 
            this.first_lesson.Location = new System.Drawing.Point(23, 223);
            this.first_lesson.Name = "first_lesson";
            this.first_lesson.Size = new System.Drawing.Size(94, 39);
            this.first_lesson.TabIndex = 37;
            this.first_lesson.Text = "9am-10am";
            this.first_lesson.UseVisualStyleBackColor = true;
            this.first_lesson.Click += new System.EventHandler(this.first_lesson_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 41;
            this.label2.Text = "Choose a date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(195, 197);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 42;
            this.label3.Text = "Pick a time";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 288);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 43;
            this.label4.Text = "Appointment: ";
            // 
            // ClientBooking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 422);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fourth_lesson);
            this.Controls.Add(this.second_lesson);
            this.Controls.Add(this.thrid_lesson);
            this.Controls.Add(this.first_lesson);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.submit_btn);
            this.Controls.Add(this.instructor_box);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Name = "ClientBooking";
            this.Text = "ClientBooking";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox instructor_box;
        private System.Windows.Forms.Button submit_btn;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button fourth_lesson;
        private System.Windows.Forms.Button second_lesson;
        private System.Windows.Forms.Button thrid_lesson;
        private System.Windows.Forms.Button first_lesson;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}