﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace d2
{
    public partial class scheduling : Form
    {
        public static string firstname;
        public static string lastname;
        public static string make;
        public static string license;
        public scheduling()
        {

            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            roster register = new roster();
            register.ShowDialog();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            schedulecars register = new schedulecars(firstname, lastname, make, license);
            register.ShowDialog();
            this.Close();
        }
    }
}
