﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace d2
{

    public partial class ClientBooking : Form
    {
        public static string fname;
        public static string lname;
        public ClientBooking(string firstname, string lastname)
        {
            InitializeComponent();
            string fullname;
            fname = firstname;
            lname = lastname;
            fullname = $"{firstname} {lastname}";

            textBox1.Text = fullname;

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=BCS303A-08A;Database=Deliverable2_AmyMcIntyre;Integrated Security=True")) //CHANGE THIS IF YOU HAVE DIFFERENT DATABASE NAME
            {
                SqlCommand sqlCmd = new SqlCommand("SELECT FNAME, LNAME FROM INSTRUCTOR", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();
                while (sqlReader.Read())
                {
                    instructor_box.Items.Add(sqlReader["FNAME"].ToString()+" "+(sqlReader["LNAME"].ToString()));
                }
                sqlReader.Close();
            }
        }


        public void submit_btn_Click(object sender, EventArgs e)

        {
            string  clientname = "", time = "", date = "", instructorname = "";

            time = timeslot;
            date = dateTimePicker1.Text;
            clientname = textBox1.Text;
            instructorname = instructor_box.Text;
            
            try
            {
                SQL.executeQuery("INSERT INTO TIMESLOT VALUES ('" + clientname + "', '" + time + "', '" + date + "', '" + instructorname + "' )");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }

            }
        

        public static string timeslot;
        public static string day;
        public static string instructor;
        public static string client;

        private void first_lesson_Click(object sender, EventArgs e)
        {
            timeslot = "9:00am - 10:00am";
            label4.Text = dateTimePicker1.Text.ToString() + "From: " + timeslot;
        }

        private void second_lesson_Click(object sender, EventArgs e)
        {
            timeslot = "11:00am - 12:00pm";
            label4.Text = dateTimePicker1.Text.ToString() + "From: " + timeslot;
        }

        private void thrid_lesson_Click(object sender, EventArgs e)
        {
            timeslot = "1:00pm - 2:00pm";
            label4.Text = dateTimePicker1.Text.ToString() + "From: " + timeslot;
        }

        private void fourth_lesson_Click(object sender, EventArgs e)
        {
            timeslot = "3:00pm - 4:00pm";
            label4.Text = dateTimePicker1.Text.ToString() + "From: " + timeslot;
        }

        private void instructor_box_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePicker1.Text.Contains("Sunday"))
            {
                MessageBox.Show("Sorry, We are not open on Sundays. Please choose another appointment from Monday-Saturday");
                dateTimePicker1.Text = dateTimePicker1.Value.AddDays(1).ToString();

            }

        }
    }
}
