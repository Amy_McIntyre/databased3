﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace d2
{
    //Student Name: Amy McIntyre
    //Student ID: 9999413
    public partial class AdminRegister : Form
    {
        public AdminRegister()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string username = "", password = "", firstname = "", lastname = "", email = "";
            
            try
            {
                username = textBox1.Text.Trim();
                password = textBox2.Text.Trim();
                firstname = textBox3.Text.Trim();
                lastname = textBox4.Text.Trim();
                email = textBox5.Text.Trim();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            try
            {
                SQL.executeQuery("INSERT INTO ADMIN VALUES ('" + username + "', '" + password + "', '" + firstname + "', '" + lastname + "', '" + email + "')");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }

            
            MessageBox.Show("Successfully Registered: " + firstname + " " + lastname + ". Your username is: " + username);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Welcome register = new Welcome();
            register.ShowDialog();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin register = new Admin();
            register.ShowDialog();
            this.Close();
        }
    }
}
