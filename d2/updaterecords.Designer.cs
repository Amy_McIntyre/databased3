﻿namespace d2
{
    partial class updaterecords
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.year_box = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.instructor_box = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.month_box = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.day_box = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.time_box = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // year_box
            // 
            this.year_box.FormattingEnabled = true;
            this.year_box.Items.AddRange(new object[] {
            "2017"});
            this.year_box.Location = new System.Drawing.Point(237, 287);
            this.year_box.Name = "year_box";
            this.year_box.Size = new System.Drawing.Size(121, 21);
            this.year_box.TabIndex = 27;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(237, 110);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 20);
            this.textBox1.TabIndex = 26;
            // 
            // instructor_box
            // 
            this.instructor_box.FormattingEnabled = true;
            this.instructor_box.Items.AddRange(new object[] {
            "John Mills",
            "Ashley Hanson",
            "Bill James",
            "Jess Telly"});
            this.instructor_box.Location = new System.Drawing.Point(237, 323);
            this.instructor_box.Name = "instructor_box";
            this.instructor_box.Size = new System.Drawing.Size(121, 21);
            this.instructor_box.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(80, 326);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(131, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Please select an instructor";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(80, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Client Name";
            // 
            // month_box
            // 
            this.month_box.FormattingEnabled = true;
            this.month_box.Items.AddRange(new object[] {
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sept",
            "Oct",
            "Nov",
            "Dec"});
            this.month_box.Location = new System.Drawing.Point(237, 239);
            this.month_box.Name = "month_box";
            this.month_box.Size = new System.Drawing.Size(121, 21);
            this.month_box.TabIndex = 22;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(80, 287);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Year";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(77, 247);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Choose the month";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(77, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Choose the day";
            // 
            // day_box
            // 
            this.day_box.FormattingEnabled = true;
            this.day_box.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.day_box.Location = new System.Drawing.Point(237, 196);
            this.day_box.Name = "day_box";
            this.day_box.Size = new System.Drawing.Size(121, 21);
            this.day_box.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(77, 160);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Choose the time";
            // 
            // time_box
            // 
            this.time_box.FormattingEnabled = true;
            this.time_box.Items.AddRange(new object[] {
            "09:00:00",
            "11:00:00",
            "01:00:00",
            "03:00:00"});
            this.time_box.Location = new System.Drawing.Point(237, 157);
            this.time_box.Name = "time_box";
            this.time_box.Size = new System.Drawing.Size(121, 21);
            this.time_box.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(169, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Update Records";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(155, 402);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 23);
            this.button1.TabIndex = 29;
            this.button1.Text = "Save Changes";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // updaterecords
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 455);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.year_box);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.instructor_box);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.month_box);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.day_box);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.time_box);
            this.Name = "updaterecords";
            this.Text = "updaterecords";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox year_box;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox instructor_box;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox month_box;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox day_box;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox time_box;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    }
}