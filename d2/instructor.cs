﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace d2
{
    //Student Name: Amy McIntyre
    //Student ID: 9999413
    public partial class instructor : Form
    {
        public instructor()
        {
            InitializeComponent();
            textBoxPass.PasswordChar = '*';


        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool loggedIn = false;
            string username = "", password = "", firstname = "", lastname = "";
            
            if ("".Equals(textBoxUser.Text.Trim()) || "".Equals(textBoxPass.Text.Trim()))
            {
                MessageBox.Show("Please make sure you enter a Username and Password");
                return;
            }
            
            try
            {
                username = textBoxUser.Text.Trim();
                password = textBoxPass.Text.Trim();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            SQL.selectQuery("select * FROM INSTRUCTOR");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (username.Equals(SQL.read[0].ToString()) && password.Equals(SQL.read[1].ToString()))
                    {
                        loggedIn = true;
                        firstname = SQL.read[2].ToString();
                        lastname = SQL.read[3].ToString();
                        
                        this.Hide();
                        appointments register = new appointments(firstname, lastname);
                        register.ShowDialog();
                        this.Close();
                    }
                }

                if (loggedIn)
                {
                    MessageBox.Show("Successfully logged in as: " + firstname + " " + lastname);
                }
                else
                {
                    MessageBox.Show("Login attempt unsuccessful! Please check details");
                    textBoxUser.Focus();
                    return;
                }
            }
            else
            {
                MessageBox.Show("There are no accounts registed");
                return;
            }
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Welcome register = new Welcome();
            register.ShowDialog();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            instructorregister register = new instructorregister();
            register.ShowDialog();
            this.Close();
        }
    }
}
