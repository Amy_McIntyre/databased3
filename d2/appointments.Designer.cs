﻿namespace d2
{
    partial class appointments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.button1 = new System.Windows.Forms.Button();
            this.ClientName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Time = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Day = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.InstructorName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.delete_records = new System.Windows.Forms.Button();
            this.update_records = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ClientName,
            this.Time,
            this.Day,
            this.InstructorName});
            this.listView1.Location = new System.Drawing.Point(12, 74);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(568, 254);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(214, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(156, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Load Appointments";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ClientName
            // 
            this.ClientName.Text = "Client Name";
            this.ClientName.Width = 100;
            // 
            // Time
            // 
            this.Time.Text = "Time";
            this.Time.Width = 110;
            // 
            // Day
            // 
            this.Day.Text = "Day";
            this.Day.Width = 150;
            // 
            // InstructorName
            // 
            this.InstructorName.Text = "Instructor Name";
            this.InstructorName.Width = 100;
            // 
            // delete_records
            // 
            this.delete_records.Location = new System.Drawing.Point(318, 363);
            this.delete_records.Name = "delete_records";
            this.delete_records.Size = new System.Drawing.Size(138, 39);
            this.delete_records.TabIndex = 2;
            this.delete_records.Text = "Delete";
            this.delete_records.UseVisualStyleBackColor = true;
            this.delete_records.Click += new System.EventHandler(this.delete_records_Click);
            // 
            // update_records
            // 
            this.update_records.Location = new System.Drawing.Point(132, 363);
            this.update_records.Name = "update_records";
            this.update_records.Size = new System.Drawing.Size(156, 39);
            this.update_records.TabIndex = 3;
            this.update_records.Text = "Block out appointments";
            this.update_records.UseVisualStyleBackColor = true;
            this.update_records.Click += new System.EventHandler(this.update_records_Click);
            // 
            // appointments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 440);
            this.Controls.Add(this.update_records);
            this.Controls.Add(this.delete_records);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listView1);
            this.Name = "appointments";
            this.Text = "appointments";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ColumnHeader ClientName;
        private System.Windows.Forms.ColumnHeader Time;
        private System.Windows.Forms.ColumnHeader Day;
        private System.Windows.Forms.ColumnHeader InstructorName;
        private System.Windows.Forms.Button delete_records;
        private System.Windows.Forms.Button update_records;
    }
}